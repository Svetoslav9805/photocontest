package com.example.photocontest.mappers;

import com.example.photocontest.models.ContestEntry;
import com.example.photocontest.models.DTO.CreateEntryDTO;
import com.example.photocontest.models.DTO.EntryOutputDTO;
import com.example.photocontest.models.DTO.UpdateEntryDTO;
import com.example.photocontest.models.User;
import com.example.photocontest.services.contracts.ContestEntryService;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.services.contracts.UserService;
import org.springframework.stereotype.Component;

@Component
public class EntryModelMapper {

    private final UserService userService;
    private final ContestService contestService;
    private final ContestEntryService contestEntryService;

    public EntryModelMapper(ContestService contestService,
                            ContestEntryService contestEntryService, UserService userService) {
        this.contestService = contestService;
        this.contestEntryService = contestEntryService;
        this.userService = userService;
    }

    public ContestEntry dtoToEntry(CreateEntryDTO createEntryDTO, User user){
        ContestEntry entry = new ContestEntry();
        entry.setUser(user);
        entry.setTitle(createEntryDTO.getTitle());
        entry.setStory(createEntryDTO.getStory());
        entry.setContest(contestService.getById(createEntryDTO.getContest_id()));
        return entry;
    }

    public ContestEntry updateDtoToEntry(UpdateEntryDTO updateEntryDTO) {
        ContestEntry entry = contestEntryService.getById(updateEntryDTO.getEntryId());
        entry.setTitle(updateEntryDTO.getTitle());
        entry.setStory(updateEntryDTO.getStory());
        return entry;
    }

    public ContestEntry dtoToEntry (EntryOutputDTO entryOutputDTO) {
        ContestEntry contestEntry = new ContestEntry();
        contestEntry.setTitle(entryOutputDTO.getTitle());
        contestEntry.setStory(entryOutputDTO.getStory());
        contestEntry.setUser(userService.getById(entryOutputDTO.getUserId()));
        contestEntry.setContest(contestService.getById(entryOutputDTO.getContest_id()));
        return contestEntry;
    }
}
