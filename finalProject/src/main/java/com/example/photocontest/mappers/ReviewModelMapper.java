package com.example.photocontest.mappers;

import com.example.photocontest.models.DTO.ReviewDTO;
import com.example.photocontest.models.Review;
import com.example.photocontest.services.contracts.ContestEntryService;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReviewModelMapper {

    private final UserService userService;
    private final ContestService contestService;
    private final ContestEntryService contestEntryService;

    @Autowired
    public ReviewModelMapper(UserService userService,
                             ContestService contestService,
                             ContestEntryService contestEntryService) {
        this.userService = userService;
        this.contestService = contestService;
        this.contestEntryService = contestEntryService;
    }

    public Review reviewDtoToReview(ReviewDTO reviewDTO, int contestId) {
        Review review = new Review();
        review.setScore(reviewDTO.getScore());
        review.setComment(reviewDTO.getComment());
        review.setContest(contestService.getById(contestId));
        review.setUser(userService.getById(reviewDTO.getUserId()));
        review.setPhoto(contestEntryService.getById(reviewDTO.getEntryId()).getPhoto());
        return review;
    }
}
