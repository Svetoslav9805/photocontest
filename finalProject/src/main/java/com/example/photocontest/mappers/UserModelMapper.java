package com.example.photocontest.mappers;

import com.example.photocontest.models.DTO.*;

import com.example.photocontest.models.User;
import com.example.photocontest.models.enums.UserRole;
import com.example.photocontest.repositories.contracts.UserRepository;
import com.example.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserModelMapper {

    private final UserRepository userRepository;
    private final UserService userService;

    public static final int DEFAULT_SCORE = 0;

    @Autowired
    public UserModelMapper(UserRepository userRepository, UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }

    public User userDtoToUser(CreateUserDTO userDto) {
        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
        user.setUserRole(UserRole.PHOTO_JUNKIE);
        user.setPoints(DEFAULT_SCORE);

        return user;
    }

    public User userDtoToUser(RegisterDTO registerDTO) {
        User user = new User();
        user.setFirstName(registerDTO.getFirstName());
        user.setLastName(registerDTO.getLastName());
        user.setUsername(registerDTO.getUsername());
        user.setPassword(registerDTO.getPassword());
        user.setEmail(registerDTO.getEmail());
        user.setUserRole(UserRole.PHOTO_JUNKIE);
        user.setPoints(DEFAULT_SCORE);

        return user;
    }

    public User userDtoToUser(UpdateUserDTO userDto, int userId) {
        User user = userService.getById(userId);
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        return user;
    }

    public User dtoToObject(UpdateUserDTO userDto, int id) {
        User user = userRepository.getById(id);
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        return user;
    }

    public User fromDto(CreateUserDTO userDTO, int id) {
        User userToUpdate = userRepository.getById(id);
        fromDtoToObject(userDTO, userToUpdate);
        return userToUpdate;
    }

    private void fromDtoToObject(CreateUserDTO userDTO, User userToUpdate) {
        userToUpdate.setUsername(userDTO.getUsername());
        userToUpdate.setFirstName(userDTO.getFirstName());
        userToUpdate.setLastName(userDTO.getLastName());
    }


    public UpdateUserDTO userToUpdateDto(User user) {

        UpdateUserDTO userDto = new UpdateUserDTO();
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        return userDto;
    }


    public JuryDTO userToJuryDto(User user) {

        JuryDTO juryDto = new JuryDTO();
        juryDto.setName(user.getFirstName() + " " + user.getLastName());
        juryDto.setPoints(user.getPoints());
        juryDto.setUserID(user.getUserId());
        return juryDto;
    }

    public UserOutputDTO userToDto(User user) {

        UserOutputDTO userOutputDto = new UserOutputDTO();
        userOutputDto.setFirstName(user.getFirstName());
        userOutputDto.setLastName(user.getLastName());
        userOutputDto.setRole(user.getUserRole().toString());
        userOutputDto.setScore(user.getPoints());
        userOutputDto.setId(user.getUserId());

        return userOutputDto;

    }
}
