package com.example.photocontest.mappers;

import com.example.photocontest.models.ContestCategory;
import com.example.photocontest.models.DTO.CategoryDTO;
import org.springframework.stereotype.Component;

@Component
public class CategoryModelMapper {
    public ContestCategory dtoToCategory(CategoryDTO categoryDto) {
        ContestCategory category = new ContestCategory();
        category.setName(categoryDto.getCategoryName());
        return category;
    }
}
