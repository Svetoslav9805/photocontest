package com.example.photocontest.mappers;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.DTO.ContestOutputDTO;
import com.example.photocontest.models.DTO.CreateContestDTO;
import com.example.photocontest.models.Photo;
import com.example.photocontest.models.User;
import com.example.photocontest.services.contracts.ContestCategoryService;
import com.example.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ContestModelMapper {

    private final UserService userService;
    private final ContestCategoryService categoryService;

    @Autowired
    public ContestModelMapper(ContestCategoryService categoryService, UserService userService) {
        this.categoryService = categoryService;
        this.userService = userService;
    }

    public Contest dtoToContest(CreateContestDTO createContestDTO) {
        Contest contest = new Contest();

        contest.setTitle(createContestDTO.getTitle());
        contest.setContestCategory(categoryService.getById(createContestDTO.getCategoryType()));
        contest.setPhase_1 (LocalDateTime.of(createContestDTO.getDaysPhase1().toLocalDate(), LocalTime.of(0, 0)));
        contest.setPhase_2(contest.getPhase_1().plusHours(createContestDTO.getHoursPhase2()));
        contest.setOpen(true);

        if (createContestDTO.getJuryList() == null) {
            contest.setJurySet(new HashSet<>());
        }
        contest.setJurySet(createContestDTO.getJuryList().stream()
                .map(userService::getById)
                .collect(Collectors.toSet()));

        if (createContestDTO.getParticipantsSet() == null) {
            contest.setParticipantsSet(new HashSet<>());
        } else {
            contest.setParticipantsSet(createContestDTO.getParticipantsSet().stream()
                    .map(userService::getById)
                    .collect(Collectors.toSet()));
        }
        return contest;
    }

    public ContestOutputDTO contestToDto(Contest contest) {
        ContestOutputDTO outputContest = new ContestOutputDTO();
        outputContest.setCategory(contest.getContestCategory().getName());
        outputContest.setDaysPhase1(contest.getPhase_1().toLocalDate());
        outputContest.setHoursPhase2(contest.getPhase_2());
        outputContest.setOpen(contest.getOpen());
        outputContest.setTitle(contest.getTitle());
        outputContest.setPhotoList(contest.getContestPhotos());
        outputContest.setCoverPhoto(contest.getCoverPhoto());
        outputContest.setJuryList(contest.getJurySet().stream().map(User::getUserId).collect(Collectors.toList()));
        outputContest.setParticipantList(contest.getParticipantsSet().stream().map(User::getUserId).collect(Collectors.toList()));
        outputContest.setFinished(contest.isFinished());
        outputContest.setId(contest.getId());
        return outputContest;
    }
}

