package com.example.photocontest.models.DTO;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateEntryDTO {

    @NotNull(message = "Contest id cannot be null.")
    private int entryId;

    @NotNull(message = "Title cannot be null.")
    @Size(max = 64, message = "Title shouldn't be longer than 64 characters.")
    private String title;

    @NotNull(message = "Story cannot be null.")
    private String story;

    public UpdateEntryDTO(int entryId, String title, String story) {
        this.entryId = entryId;
        this.title = title;
        this.story = story;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }
}
