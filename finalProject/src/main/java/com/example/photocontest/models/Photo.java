package com.example.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "photo")
public class Photo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int photoId;

    @Column(name = "file_path")
    private String filePath;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User author;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "photo", cascade = CascadeType.ALL)
    private List<Review> reviewSet;


    public Photo() {
    }

    public Photo(int photoId, String filePath, User author) {
        this.photoId = photoId;
        this.filePath = filePath;
        this.author = author;
    }

    @Transient
    public int getPoints() {
        int points = 0;
        for (Review review : getReviewSet()) {
            points = points + review.getScore();
        }
        return points;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public User getAuthor() {
        return author;
    }

    public List<Review> getReviewSet() {
        return reviewSet;
    }

    public void setReviewSet(List<Review> reviewSet) {
        this.reviewSet = reviewSet;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

}
