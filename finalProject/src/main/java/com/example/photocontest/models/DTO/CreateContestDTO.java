package com.example.photocontest.models.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.Range;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import java.sql.Date;
import java.util.Set;

public class CreateContestDTO {

    public static final String PHASE_ONE_MIN_ERROR = "Phase one can be minimum 1 day.";
    public static final String PHASE_ONE_MAX_ERROR = "Phase one can be max 30 days.";
    public static final String PHASE_TWO_MIN_ERROR = "Phase two can be minimum 1 hour.";
    public static final String PHASE_TWO_MAX_ERROR = "Phase two can be max 24 hours.";

    @NotNull(message = "Title cannot be null.")
    @Size(max = 64, message = "Title shouldn't be longer than 64 characters.")
    private String title;

    @NotNull
    private int categoryType;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date daysPhase1;

    @NotNull
    @Range(min = 1, max = 24, message = "Should be between 1 and 24 hours")
    private int hoursPhase2;

    private Set<Integer> juryList;

    private Set<Integer> participantsSet;

    public CreateContestDTO() {
    }

    public CreateContestDTO(String title, int categoryType, Date daysPhase1,
                            int hoursPhase2, Set<Integer> juryList, Set<Integer> participantsSet) {
        this.title = title;
        this.categoryType = categoryType;
        this.daysPhase1 = daysPhase1;
        this.hoursPhase2 = hoursPhase2;
        this.juryList = juryList;
        this.participantsSet = participantsSet;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(int categoryType) {
        this.categoryType = categoryType;
    }

    public Date getDaysPhase1() {
        return daysPhase1;
    }

    public void setDaysPhase1(Date daysPhase1) {
        this.daysPhase1 = daysPhase1;
    }

    public int getHoursPhase2() {
        return hoursPhase2;
    }

    public void setHoursPhase2(int hoursPhase2) {
        this.hoursPhase2 = hoursPhase2;
    }

    public Set<Integer> getJuryList() {
        return juryList;
    }

    public void setJuryList(Set<Integer> juryList) {
        this.juryList = juryList;
    }

    public Set<Integer> getParticipantsSet() {
        return participantsSet;
    }

    public void setParticipantsSet(Set<Integer> participantsSet) {
        this.participantsSet = participantsSet;
    }
}
