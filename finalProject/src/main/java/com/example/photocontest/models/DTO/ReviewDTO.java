package com.example.photocontest.models.DTO;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class ReviewDTO {

    @NotNull
    int userId;

    int entryId;

    @NotNull
    @Size(min = 10, max = 8192, message = "Comment should be between 32 and 8192 symbols")
    private String comment;

    @NotNull
    @PositiveOrZero
    private int score;

    public ReviewDTO() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }
}


