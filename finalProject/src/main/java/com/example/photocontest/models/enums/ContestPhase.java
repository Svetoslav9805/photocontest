package com.example.photocontest.models.enums;

public enum ContestPhase {
    NOT_STARTED("Not started"),
    PHASE_I("Enrollment"),
    PHASE_II("Evaluation"),
    FINISHED("Finished");

    String phaseAsString;

    ContestPhase(String phaseAsString) {
        this.phaseAsString = phaseAsString;
    }

    @Override
    public String toString() {
        return this.phaseAsString;
    }
}
