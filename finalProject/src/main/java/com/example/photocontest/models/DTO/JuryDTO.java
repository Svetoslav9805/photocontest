package com.example.photocontest.models.DTO;

public class JuryDTO {

    private int userID;

    private String name;

    private int points;

    public JuryDTO() {
    }

    public JuryDTO(int userId, String name, int points) {
        this.userID = userId;
        this.name = name;
        this.points = points;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
