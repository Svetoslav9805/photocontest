package com.example.photocontest.models.DTO;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateUserDTO {

    @NotNull(message = "First name must not be null")
    @Size(min = 4, max = 32, message = "First name should be between 4 and 32 symbols.")
    private String firstName;

    @NotNull(message = "Last name must not be null")
    @Size(min = 4, max = 32, message = "Last name should be between 4 and 32 symbols.")
    private String lastName;

    @NotNull(message = "Username must not be null")
    @Size(min = 2, max = 20, message = "Username should be between 2 and 20 symbols.")
    private String username;

    @NotNull(message = "Password must not be null")
    @Size(min = 2, max = 20, message = "Password should be between 2 and 20 symbols.")
    private String password;

    @NotNull(message = "Email must not be null")
    @Size(min = 2, max = 20, message = "Email should be between 2 and 20 symbols.")
    private String email;

    public CreateUserDTO() {
    }

    public CreateUserDTO(String username, String password, String firstName, String lastName, String email) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
