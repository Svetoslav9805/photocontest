package com.example.photocontest.models.enums;

public enum UserRole {
    PHOTO_JUNKIE,
    ORGANIZER
}
