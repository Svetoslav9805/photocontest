package com.example.photocontest.models.DTO;

import com.example.photocontest.models.ContestEntry;
import com.example.photocontest.models.Photo;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public class ContestOutputDTO {
    private int id;

    private String title;

    private String category;

    private LocalDate daysPhase1;

    private LocalDateTime hoursPhase2;

    private boolean isOpen;

    private boolean finished;

    @JsonIgnore
    private Set<Photo> photoList;

    private String coverPhoto;

    private List<Integer> juryList;

    private List<Integer> participantList;

    public ContestOutputDTO() {
    }

    public ContestOutputDTO(int id, String title, String category, LocalDate daysPhase1, LocalDateTime hoursPhase2,
                            boolean isOpen, boolean finished, Set<Photo> photoList, String coverPhoto, List<Integer> juryList,
                            List<Integer> participantList) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.daysPhase1 = daysPhase1;
        this.hoursPhase2 = hoursPhase2;
        this.isOpen = isOpen;
        this.finished = finished;
        this.photoList = photoList;
        this.coverPhoto = coverPhoto;
        this.juryList = juryList;
        this.participantList = participantList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public LocalDate getDaysPhase1() {
        return daysPhase1;
    }

    public void setDaysPhase1(LocalDate daysPhase1) {
        this.daysPhase1 = daysPhase1;
    }

    public LocalDateTime getHoursPhase2() {
        return hoursPhase2;
    }

    public void setHoursPhase2(LocalDateTime hoursPhase2) {
        this.hoursPhase2 = hoursPhase2;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public Set<Photo> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(Set<Photo> photoList) {
        this.photoList = photoList;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public List<Integer> getJuryList() {
        return juryList;
    }

    public void setJuryList(List<Integer> juryList) {
        this.juryList = juryList;
    }

    public List<Integer> getParticipantList() {
        return participantList;
    }

    public void setParticipantList(List<Integer> participantList) {
        this.participantList = participantList;
    }
}
