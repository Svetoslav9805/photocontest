package com.example.photocontest.models.DTO;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EntryOutputDTO {

    @NotNull(message = "Title cannot be null.")
    @Size(max = 64, message = "Title shouldn't be longer than 64 characters.")
    private String title;

    @NotNull(message = "Story cannot be null.")
    private String story;


    private Integer contest_id;

    private int userId;

    private MultipartFile photoFile;

    public EntryOutputDTO() {
    }

    public EntryOutputDTO(String title, String story, Integer contest_id,int userId) {
        this.title = title;
        this.story = story;
        this.contest_id = contest_id;
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public Integer getContest_id() {
        return contest_id;
    }

    public int getUserId() {
        return userId;
    }

    public MultipartFile getPhotoFile(){
        return photoFile;
    }

    public void setPhotoFile(MultipartFile photoFile) {
        this.photoFile = photoFile;
    }
        public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setContest_id(Integer contest_id) {
        this.contest_id = contest_id;
    }
}
