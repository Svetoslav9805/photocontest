package com.example.photocontest.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "contests_categories")
public class ContestCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotNull(message = "Category cannot be null")
    @Size(min = 2, max = 20, message = "Category should be between 2 and 50 symbols")
    @Column(name = "category")
    private String name;

    public ContestCategory() {
    }

    public ContestCategory(String name) {
        this.name = name;
    }

    public ContestCategory(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
