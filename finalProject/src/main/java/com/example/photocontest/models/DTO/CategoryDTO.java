package com.example.photocontest.models.DTO;

import com.sun.istack.NotNull;

import javax.validation.constraints.Size;

public class CategoryDTO {

    @NotNull
    @Size(min = 2, max = 100, message = "Category should be between 2 and 100")
    private String categoryName;

    public CategoryDTO() {
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
