package com.example.photocontest.models;

import com.example.photocontest.models.enums.ContestPhase;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDate;
import java.time.chrono.ChronoLocalDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "contests")
public class Contest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "title")
    private String title;

    @OneToOne
    @JoinColumn(name = "category_id")
    private ContestCategory contestCategory;

    @Column(name = "time_limit_phase_1")
    private LocalDateTime phase_1;

    @Column(name = "time_limit_phase_2")
    private LocalDateTime phase_2;

    @Column(name = "cover_photo")
    private String coverPhoto;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinTable(
            name = "contests_participants",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> participantsSet;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinTable(
            name = "contests_juries",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> jurySet;


    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "contest_entries",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "photo_id"))
    private Set<Photo> contestPhotos;

    @Column(name = "is_open")
    private boolean isOpen;

    @Column(name = "is_finished")
    private boolean finished;

    public Contest() {
    }

    public Contest(int id, String title, ContestCategory contestCategory,
                   LocalDateTime phase_1, LocalDateTime phase_2, String coverPhoto,
                   Set<User> participantsSet, Set<User> jurySet,
                   Set<Photo> contestPhotos, boolean isOpen, boolean finished) {
        this.id = id;
        this.title = title;
        this.contestCategory = contestCategory;
        this.phase_1 = phase_1;
        this.phase_2 = phase_2;
        this.coverPhoto = coverPhoto;
        this.participantsSet = participantsSet;
        this.jurySet = jurySet;
        this.contestPhotos = contestPhotos;
        this.isOpen = isOpen;
        this.finished = finished;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ContestCategory getContestCategory() {
        return contestCategory;
    }

    public void setContestCategory(ContestCategory contestCategory) {
        this.contestCategory = contestCategory;
    }

    public LocalDateTime getPhase_1() {
        return phase_1;
    }

    public void setPhase_1(LocalDateTime phase_1) {
        this.phase_1 = phase_1;
    }

    public LocalDateTime getPhase_2() {
        return phase_2;
    }

    public void setPhase_2(LocalDateTime phase_2) {
        this.phase_2 = phase_2;
    }

    public Set<User> getParticipantsSet() {
        return participantsSet;
    }

    public void setParticipantsSet(Set<User> participantsSet) {
        this.participantsSet = participantsSet;
    }

    public Set<User> getJurySet() {
        return jurySet;
    }

    public void setJurySet(Set<User> jurySet) {
        this.jurySet = jurySet;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public ContestPhase getPhase() {
        if (LocalDateTime.now().isBefore(phase_1)) {
            return ContestPhase.NOT_STARTED;
        }
        if (LocalDateTime.now().isBefore(phase_2)){
            return ContestPhase.PHASE_I;
        }
        if (LocalDateTime.now().isAfter(phase_2)){
            return ContestPhase.PHASE_II;
        }
        return ContestPhase.FINISHED;
    }

    public Set<Photo> getContestPhotos() {
        return contestPhotos;
    }

    public void setContestPhotos(Set<Photo> contestPhotos) {
        this.contestPhotos = contestPhotos;
    }

}
