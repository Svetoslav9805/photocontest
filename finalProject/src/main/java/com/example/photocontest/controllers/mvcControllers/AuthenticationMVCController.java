package com.example.photocontest.controllers.mvcControllers;

import com.example.photocontest.exceptions.AuthenticationException;
import com.example.photocontest.exceptions.DuplicateEntityException;
import com.example.photocontest.mappers.UserModelMapper;
import com.example.photocontest.models.DTO.CreateUserDTO;
import com.example.photocontest.models.DTO.LoginDTO;
import com.example.photocontest.models.DTO.RegisterDTO;
import com.example.photocontest.models.User;
import com.example.photocontest.services.contracts.UserService;
import com.example.photocontest.utils.AuthenticationHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/auth")
public class AuthenticationMVCController {
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserModelMapper userModelMapper;

    public AuthenticationMVCController(UserService userService, AuthenticationHelper authenticationHelper, UserModelMapper userModelMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userModelMapper = userModelMapper;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("loginDTO", new LoginDTO());
        return "login";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("registerDTO", new RegisterDTO());
        return "register";
    }


    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("loginDTO") LoginDTO login,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "login";
        }
        try {
            authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            session.setAttribute("currentUser", login.getUsername());
            User loggedUser = userService.getByUsername(login.getUsername());
            session.setAttribute("loggedId", loggedUser.getUserId());
            session.setAttribute("loggedFirstName", loggedUser.getFirstName());
            return "redirect:/";
        } catch (AuthenticationException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }



    @PostMapping("/register")
    public String register(@Valid @ModelAttribute("registerDTO") RegisterDTO registerDTO,
                           BindingResult errors,
                           HttpSession session,
                           Model model) {
        if (errors.hasErrors()) {
            return "/register";
        }
        try {
            User newUser = userModelMapper.userDtoToUser(registerDTO);
            userService.create(newUser);
            return "redirect:/";
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "register";
        }
    }
}
