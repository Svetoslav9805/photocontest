package com.example.photocontest.controllers.restControllers;

import com.example.photocontest.mappers.ContestModelMapper;
import com.example.photocontest.mappers.EntryModelMapper;
import com.example.photocontest.mappers.ReviewModelMapper;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestEntry;
import com.example.photocontest.models.DTO.CreateContestDTO;
import com.example.photocontest.models.DTO.CreateEntryDTO;
import com.example.photocontest.models.DTO.ReviewDTO;
import com.example.photocontest.models.DTO.UpdateEntryDTO;
import com.example.photocontest.models.Review;
import com.example.photocontest.models.User;
import com.example.photocontest.services.contracts.ContestEntryService;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.services.contracts.ReviewService;
import com.example.photocontest.utils.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/contest")
public class ContestController {

    private final ReviewService reviewService;
    private final ContestService contestService;
    private final EntryModelMapper entryModelMapper;
    private final ReviewModelMapper reviewModelMapper;
    private final ContestModelMapper contestModelMapper;
    private final ContestEntryService contestEntryService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ContestController(ReviewService reviewService,
                             ContestService contestService,
                             EntryModelMapper entryModelMapper,
                             ReviewModelMapper reviewModelMapper,
                             ContestModelMapper contestModelMapper,
                             ContestEntryService contestEntryService,
                             AuthenticationHelper authenticationHelper) {
        this.reviewService = reviewService;
        this.contestService = contestService;
        this.entryModelMapper = entryModelMapper;
        this.reviewModelMapper = reviewModelMapper;
        this.contestModelMapper = contestModelMapper;
        this.contestEntryService = contestEntryService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("{id}")
    public Contest getContestById(@PathVariable int id,
                                  @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return contestService.getById(id);
    }

    @PostMapping(consumes = {"multipart/form-data", "application/json"})
    public void createContest(@RequestHeader HttpHeaders headers,
                              @RequestPart("contestCreateDto") CreateContestDTO createContestDTO,
                              @RequestParam("image") MultipartFile image) {

        User user = authenticationHelper.tryGetUser(headers);
        Contest contest = contestModelMapper.dtoToContest(createContestDTO);
        contestService.createContest(contest, image, user);
    }

    @DeleteMapping("/{contestId}")
    public void deleteContest(@RequestHeader HttpHeaders headers, @PathVariable int contestId) {
        User user = authenticationHelper.tryGetUser(headers);
        contestService.deleteContest(contestId);
    }

    @GetMapping("/entries/{contestId}")
    public List<ContestEntry> getAllEntriesByContestId(@PathVariable int contestId) {
        return contestEntryService.getAllByContestId(contestId);
    }

    @GetMapping(path = "/entry/{entryId}")
    public ContestEntry getContestEntryById(@RequestHeader HttpHeaders headers,
                                            @PathVariable int entryId) {
        User user = authenticationHelper.tryGetUser(headers);
        return contestEntryService.getById(entryId);
    }

    @PostMapping(path = "/entry", consumes = {"multipart/form-data", "application/json"})
    public void createContestEntry(@RequestHeader HttpHeaders headers,
                                   @RequestPart("createEntryDTO") CreateEntryDTO createEntryDTO,
                                   @RequestParam("image") MultipartFile image) {
        User user = authenticationHelper.tryGetUser(headers);
        ContestEntry entry = entryModelMapper.dtoToEntry(createEntryDTO, user);
        contestEntryService.createEntry(entry, image, user);
    }

    @PutMapping(path = "/entry/update")
    public void updateContestEntry(@RequestHeader HttpHeaders headers,
                                   @RequestBody UpdateEntryDTO updateEntryDTO) {
        User user = authenticationHelper.tryGetUser(headers);
        ContestEntry entry = entryModelMapper.updateDtoToEntry(updateEntryDTO);
        contestEntryService.updateEntry(entry, user);
    }

    @DeleteMapping("/entry/{entryId}")
    public void deleteContestEntry(@RequestHeader HttpHeaders headers,
                                   @PathVariable int entryId) {
        User user = authenticationHelper.tryGetUser(headers);
        contestEntryService.deleteEntry(entryId, user);
    }

    @PostMapping("/review/{contestId}")
    public void createEntryReview(@Valid @RequestBody ReviewDTO reviewDTO,
                                  @RequestHeader HttpHeaders headers,
                                  @PathVariable int contestId) {
        User user = authenticationHelper.tryGetUser(headers);
        Review review = reviewModelMapper.reviewDtoToReview(reviewDTO, contestId);
        reviewService.create(review);
    }

    @GetMapping("reviews/{photoId}")
    public List<Review> getAllReviewsByEntryId(@RequestHeader HttpHeaders headers,
                                                 @PathVariable int photoId) {
        User user = authenticationHelper.tryGetUser(headers);
        return reviewService.getAllReviewsByPhotoId(photoId);
    }
}
