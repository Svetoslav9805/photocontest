package com.example.photocontest.controllers.restControllers;

import com.example.photocontest.models.User;
import com.example.photocontest.services.contracts.PhotoService;
import com.example.photocontest.utils.AuthenticationHelper;
import com.example.photocontest.utils.ImageUploadHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/photo")
public class PhotoController {

    private final PhotoService photoService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PhotoController(PhotoService photoService,
                           AuthenticationHelper authenticationHelper) {
        this.photoService = photoService;
        this.authenticationHelper = authenticationHelper;
    }

    @PostMapping(consumes = {"multipart/form-data"})
    public void uploadImage(@RequestHeader HttpHeaders headers,
                            @RequestParam("image") MultipartFile multipartImage) throws Exception {
        User user = authenticationHelper.tryGetUser(headers);
        photoService.createPhoto(multipartImage, user);
    }


}
