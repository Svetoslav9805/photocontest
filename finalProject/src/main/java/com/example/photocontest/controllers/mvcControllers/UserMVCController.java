package com.example.photocontest.controllers.mvcControllers;

import com.example.photocontest.exceptions.AuthenticationException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorisedException;
import com.example.photocontest.mappers.UserModelMapper;
import com.example.photocontest.models.DTO.UpdateUserDTO;
import com.example.photocontest.models.User;
import com.example.photocontest.services.contracts.UserService;
import com.example.photocontest.utils.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UserMVCController {
    private static final String ORGANIZER_ROLE = "Organizer";

    private final UserService userService;
    private final UserModelMapper userModelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserMVCController(UserService userService, UserModelMapper userModelMapper, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userModelMapper = userModelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean isAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isOrganizer")
    public boolean populateIsOrganizer(HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser1(session);
            return loggedUser.getUserRole().ordinal() == 1;
        } catch (AuthenticationException | EntityNotFoundException | NullPointerException e) {
            return false;
        }
    }



    @GetMapping()
    public String showAllUsers(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser1(session);
        } catch (AuthenticationException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("users", userService.getAll());
        return "all-users";
    }

    @GetMapping("/{id}")
    public String showSingleUser(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser1(session);
            model.addAttribute("loggedUser", user);
        } catch (AuthenticationException e) {
            return "redirect:/auth/login";
        }
        try {
            user = userService.getById(id);
            model.addAttribute("user", user);
            return "profileInfo";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping()
    public String handleSearch(@ModelAttribute("search") Optional<String> search, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser1(session);
        } catch (AuthenticationException e) {
            return "redirect:/auth/login";
        }
        List<User> users = new ArrayList<>();
        users = userService.searchByUsername(search);
        model.addAttribute("users",users);
        return "all-users";
    }



    @GetMapping("/update/{userId}")
    public String updateProfilePage(@PathVariable int userId, Model model, HttpSession session) {
       User user = userService.getById(userId);
       UpdateUserDTO userDto = userModelMapper.userToUpdateDto(user);

        model.addAttribute("user", userDto);

        return "editProfile";
    }

    @PostMapping("/update/{userId}")
    public String updateProfile(@PathVariable int userId,
                                @Valid @ModelAttribute("user") UpdateUserDTO userDto,
                                BindingResult errors,
                                Model model,
                                HttpSession session) {
        if (errors.hasErrors()) {
            return "editProfile";
        }
        try {
            User userToBeUpdated = userModelMapper.userDtoToUser(userDto, userId);
            userService.update(userToBeUpdated);
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "createCategoryView";
        }

        return "redirect:/users/" + userId;
    }



//    @GetMapping("/{id}/delete")
//    public String deleteUser(@PathVariable int id, Model model, HttpSession session) {
//        User loggedUser;
//        try {
//            loggedUser = authenticationHelper.tryGetUser1(session);
//        } catch (AuthenticationException e) {
//            return "redirect:/auth/login";
//        }
//        try {
////            userService.delete(id, loggedUser);
//
//            return "redirect:/users";
//        } catch (EntityNotFoundException e) {
//            model.addAttribute("error", e.getMessage());
//            return "not-found";
//        } catch (UnauthorisedException e) {
//            model.addAttribute("error", e.getMessage());
//            return "access-denied";
//        }
//    }

    @GetMapping("/{id}/promote")
    public String promoteUser(@PathVariable int id, Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser1(session);
        } catch (AuthenticationException e) {
            return "redirect:/auth/login";
        }

        try {
            User user = userService.getById(id);
            userService.promote(loggedUser, user);

            return "redirect:/users/" + id;

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorisedException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }


}
