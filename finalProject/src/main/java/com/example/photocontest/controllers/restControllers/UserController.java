package com.example.photocontest.controllers.restControllers;


import com.example.photocontest.mappers.UserModelMapper;
import com.example.photocontest.models.DTO.CreateUserDTO;
import com.example.photocontest.models.DTO.UpdateUserDTO;
import com.example.photocontest.models.User;
import com.example.photocontest.services.contracts.UserService;
import com.example.photocontest.utils.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService service;
    private final UserModelMapper userModelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService service,
                          UserModelMapper userModelMapper,
                          AuthenticationHelper authenticationHelper) {

        this.service = service;
        this.userModelMapper = userModelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<User> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
        return service.getById(id);
    }

    @PostMapping
    public User create(@Valid @RequestBody CreateUserDTO userDto) {
        User user = userModelMapper.userDtoToUser(userDto);
        return service.create(user);
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders headers, @PathVariable int id,
                       @Valid @RequestBody UpdateUserDTO userUpdateDTO) {
        User updatingUser = authenticationHelper.tryGetUser(headers);
        User userToUpdate = userModelMapper.dtoToObject(userUpdateDTO, id);
        service.update(updatingUser, userToUpdate);
    }


}
