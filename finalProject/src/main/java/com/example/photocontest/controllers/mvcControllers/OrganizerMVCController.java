package com.example.photocontest.controllers.mvcControllers;

import com.example.photocontest.mappers.CategoryModelMapper;
import com.example.photocontest.mappers.ContestModelMapper;
import com.example.photocontest.mappers.UserModelMapper;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestCategory;
import com.example.photocontest.models.DTO.*;
import com.example.photocontest.services.contracts.ContestCategoryService;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.services.contracts.UserService;
import com.example.photocontest.utils.ImageUploadHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/organiser")
public class OrganizerMVCController {
    private final ContestService contestService;
    private final ContestModelMapper contestModelMapper;
    private final ContestCategoryService contestCategoryService;
    private final UserService userService;
    private final UserModelMapper userModelMapper;
    private final ImageUploadHelper imageUploadHelper;
    private final CategoryModelMapper categoryModelMapper;

    public OrganizerMVCController(ContestService contestService, ContestModelMapper contestModelMapper,
                                  ContestCategoryService contestCategoryService, UserService userService,
                                  UserModelMapper userModelMapper, ImageUploadHelper imageUploadHelper, CategoryModelMapper categoryModelMapper) {
        this.contestService = contestService;
        this.contestModelMapper = contestModelMapper;
        this.contestCategoryService = contestCategoryService;
        this.userService = userService;
        this.userModelMapper = userModelMapper;
        this.imageUploadHelper = imageUploadHelper;
        this.categoryModelMapper = categoryModelMapper;
    }

    @GetMapping()
    public String showOrganiserHomepage(Model model, HttpSession session) {
        List<ContestOutputDTO> outputDtoList = contestService.getAll().stream()
                .map(contestModelMapper::contestToDto)
                .collect(Collectors.toList());
        model.addAttribute("contests", outputDtoList);
        return "organiser";
    }



//    @GetMapping("/category")
//    public String getCreateCategoryView(Model model, HttpSession session) {
////        CategoryDto categoryDto = new CategoryDto();
////        model.addAttribute("category", categoryDto);
//
//        return "contestCreate";
//    }

    @GetMapping("/create")
    public String showCreateContestPage(Model model, HttpSession session) {
        List<ContestCategory> categories = contestCategoryService.getAllCategories();
        List<JuryDTO> possibleJury = userService.getAllJuries().stream()
                .map(userModelMapper::userToJuryDto).collect(Collectors.toList());
        CreateContestDTO contestCreateDto = new CreateContestDTO();
        model.addAttribute("categories", categories);
        model.addAttribute("juries", possibleJury);
        model.addAttribute("contestCreateDto", contestCreateDto);

        return "contestCreate";
    }

    @PostMapping("/create")
    public String createContest(@Valid @RequestParam(name="image", required=false) MultipartFile multipartFile,
                                @ModelAttribute("contestCreateDto") CreateContestDTO contestCreateDto,
                                BindingResult errors,
                                Model model,
                                HttpSession session) {

        model.addAttribute("loggedUser", (UserOutputDTO) session.getAttribute("user"));
        List<ContestCategory> categories = contestCategoryService.getAllCategories();
        List<JuryDTO> possibleJury = userService.getAllJuries().stream()
                .map(userModelMapper::userToJuryDto)
                .collect(Collectors.toList());
        int id;

        if (errors.hasErrors()) {
            model.addAttribute("categories", categories);
            model.addAttribute("juries", possibleJury);
            return "contestCreate";
        }

        try {
            Contest newContest = contestModelMapper.dtoToContest(contestCreateDto);
            String link = imageUploadHelper.upload(multipartFile);
            newContest.setCoverPhoto(link);
            contestService.create(newContest);
            id = newContest.getId();
        } catch (RuntimeException e) {
            model.addAttribute("categories", categories);
            model.addAttribute("juries", possibleJury);
            model.addAttribute("error", e.getMessage());
            return "contestCreate";
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/contests";
    }

    @GetMapping("/category")
    public String getCreateCategoryView(Model model, HttpSession session) {
        CategoryDTO categoryDto = new CategoryDTO();
        model.addAttribute("category", categoryDto);

        return "createCategoryView";
    }

    @PostMapping("/category")
    public String createCategory(@Valid @ModelAttribute("category") CategoryDTO category,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession session) {
        if (errors.hasErrors()) {
            return "createCategoryView";
        }
        try {
            contestCategoryService.addCategory(categoryModelMapper.dtoToCategory(category));
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "createCategoryView";
        }
        return "redirect:/organiser";
    }


}
