package com.example.photocontest.controllers.mvcControllers;

import com.example.photocontest.exceptions.AuthenticationException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.mappers.UserModelMapper;
import com.example.photocontest.models.User;
import com.example.photocontest.models.enums.UserRole;
import com.example.photocontest.services.contracts.ContestEntryService;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.services.contracts.UserService;
import com.example.photocontest.utils.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class HomeMVCController {
    private static final int AMOUNT_JUNKIES = 10;


    private final ContestEntryService contestEntryService;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final UserModelMapper userModelMapper;
    private final ContestService contestService;

    @Autowired
    public HomeMVCController(ContestEntryService contestEntryService, AuthenticationHelper authenticationHelper, UserService userService, UserModelMapper userModelMapper, ContestService contestService) {

        this.contestEntryService = contestEntryService;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.userModelMapper = userModelMapper;
        this.contestService = contestService;
    }


    @ModelAttribute("isAuthenticated")
    public boolean isAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isOrganizer")
    public boolean populateIsOrganizer(HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser1(session);
            return loggedUser.getUserRole().ordinal() == 1;
        } catch (AuthenticationException | EntityNotFoundException | NullPointerException e) {
            return false;
        }
    }

    @GetMapping("/")
    public String showHomePage(Model model, HttpSession session) {
//        UserOutputDTO user = (UserOutputDTO) session.getAttribute("currentUser");

        model.addAttribute("topJunkies", userService.getTopJunkies(AMOUNT_JUNKIES).stream()
                .map(userModelMapper::userToDto)
                .collect(Collectors.toList()));
        try {

            User user = authenticationHelper.tryGetUser1(session);
            //TODO: Ментора да каже
//            UserOutputDTO user = (UserOutputDTO) session.getAttribute("currentUser");



                if (user.getUserRole().toString().equals(UserRole.ORGANIZER.toString())) {
                    return "redirect:/organiser";
                }
             return "redirect:/junkie";

        } catch (AuthenticationException e) {
            return "index";
        }
    }


}
