package com.example.photocontest.controllers.mvcControllers;

import com.example.photocontest.mappers.ContestModelMapper;
import com.example.photocontest.services.contracts.ContestService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/junkie")
public class JunkieMVCController {
    private final ContestService contestService;
    private final ContestModelMapper contestModelMapper;

    public JunkieMVCController(ContestService contestService, ContestModelMapper contestModelMapper) {
        this.contestService = contestService;
        this.contestModelMapper = contestModelMapper;
    }


    @GetMapping()
    public String showJunkieHomepage(Model model, HttpSession session) {
//        int userId = ((UserOutputDto) session.getAttribute("loggedUser")).getId();
//
//        List<ContestOutputDto> outputDtoList = contestService.getFilteredContestsByUserId("available", userId).stream()
//                .map(contestModelMapper::contestToDto)
//                .collect(Collectors.toList());
//
//        model.addAttribute("contests", outputDtoList);
//        model.addAttribute("canEnroll", true);

        return "junkie";
    }
}
