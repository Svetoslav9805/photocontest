package com.example.photocontest.controllers.mvcControllers;

import com.example.photocontest.exceptions.AuthenticationException;
import com.example.photocontest.mappers.EntryModelMapper;
import com.example.photocontest.mappers.PhotoModelMapper;
import com.example.photocontest.models.ContestEntry;
import com.example.photocontest.models.DTO.EntryOutputDTO;
import com.example.photocontest.models.User;
import com.example.photocontest.services.contracts.ContestEntryService;
import com.example.photocontest.services.contracts.PhotoService;
import com.example.photocontest.utils.AuthenticationHelper;
import com.example.photocontest.utils.ImageUploadHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
@RequestMapping("/photos")
public class PhotoMVCController {
    private final PhotoService photoService;
    private final EntryModelMapper entryModelMapper;
    private final ContestEntryService contestEntryService;
    private final PhotoModelMapper photoModelMapper;
    private final AuthenticationHelper authenticationHelper;
    private final ImageUploadHelper imageUploadHelper;

    @Autowired
    public PhotoMVCController(PhotoService photoService, EntryModelMapper entryModelMapper,
                              ContestEntryService contestEntryService, PhotoModelMapper photoModelMapper,
                              AuthenticationHelper authenticationHelper, ImageUploadHelper imageUploadHelper) {
        this.photoService = photoService;
        this.entryModelMapper = entryModelMapper;
        this.contestEntryService = contestEntryService;
        this.photoModelMapper = photoModelMapper;
        this.authenticationHelper = authenticationHelper;
        this.imageUploadHelper = imageUploadHelper;
    }

    @GetMapping("/submit/{contestId}")
    public String getSubmitPhotoView(@PathVariable int contestId, Model model, HttpSession session) {

//        UserOutputDTO loggedUser = (UserOutputDTO) session.getAttribute("loggedUser");

//        if (!photoService.canUserSubmitPhoto(loggedUser.getId(), contestId) ||
//                photoService.hasUserSubmitPhotoToContest(loggedUser.getId(), contestId)) {
//            return "redirect:/";
//        }
        try {
//            entryOutputDTO.setPhotoFile(file);
            User currentUser = authenticationHelper.tryGetUser1(session);
            EntryOutputDTO submitPhoto = new EntryOutputDTO();
            submitPhoto.setContest_id(contestId);
            submitPhoto.setUserId(currentUser.getUserId());
            model.addAttribute("photo", submitPhoto);
        } catch (AuthenticationException e) {
            return "redirect:/auth/login";
        }

        return "submitPhotoView";
    }

//    @PostMapping("/submit/{contestId}")
//    public String submitPhoto(@PathVariable int contestId,
//                              @ModelAttribute("photo") PhotoCreateDto photoCreateDto,
//                              Model model, HttpSession session,
//                              BindingResult errors) {
//        Photo photo = photoModelMapper.dtoToPhoto(photoCreateDto);
//        photoService.createPhoto(photo);
//        return "redirect:/contest/" + contestId;
//    }

    @PostMapping("/submit/{contestId}")
    public String submitPhoto(@PathVariable int contestId,
                              @ModelAttribute("photo") EntryOutputDTO entryOutputDTO,
                              Model model, HttpSession session,
                              BindingResult errors, MultipartFile file) {
        if(errors.hasErrors()){
            System.out.println("asdad");
            return "submitPhotoView";
        }
        try {
            entryOutputDTO.setContest_id(contestId);
            User currentUser = authenticationHelper.tryGetUser1(session);
            ContestEntry entry = entryModelMapper.dtoToEntry(entryOutputDTO);
            contestEntryService.createEntry(entry, entryOutputDTO.getPhotoFile(),currentUser);

        } catch (AuthenticationException e) {
            return "redirect:/auth/login";
        }
        return "submitPhotoView";
    }


    @GetMapping("{id}")
    public String getPhotoCommentsView(@PathVariable int id,
                                       Model model,
                                       HttpSession session) {
//        List<ReviewOutputDto> reviews = photoService.getPhotoReviews(id).stream()
//                .map(reviewModelMapper::reviewToOutputDto)
//                .collect(Collectors.toList());
//        Photo photo = photoService.getPhotoById(id);
//
//        model.addAttribute("photo", photo);
//        model.addAttribute("reviews", reviews);

        return "photoView";
    }
}