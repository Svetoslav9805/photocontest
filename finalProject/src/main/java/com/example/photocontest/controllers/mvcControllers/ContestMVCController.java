package com.example.photocontest.controllers.mvcControllers;

import com.example.photocontest.exceptions.AuthenticationException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.mappers.ContestModelMapper;
import com.example.photocontest.mappers.EntryModelMapper;
import com.example.photocontest.mappers.PhotoModelMapper;
import com.example.photocontest.mappers.UserModelMapper;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.DTO.ContestOutputDTO;
import com.example.photocontest.models.DTO.CreatePhotoDTO;
import com.example.photocontest.models.DTO.JuryDTO;
import com.example.photocontest.models.User;
import com.example.photocontest.services.contracts.ContestEntryService;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.services.contracts.PhotoService;
import com.example.photocontest.services.contracts.UserService;
import com.example.photocontest.utils.AuthenticationHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/contests")
public class ContestMVCController {
    private static final String ORGANIZER_ROLE = "Organizer";
    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;
    private final ContestModelMapper contestModelMapper;
    private final ContestEntryService contestEntryService;
    private final EntryModelMapper entryModelMapper;
    private final PhotoService photoService;
    private final PhotoModelMapper photoModelMapper;
    private final UserService userService;
    private final UserModelMapper userModelMapper;



    public ContestMVCController(AuthenticationHelper authenticationHelper, ContestService contestService, ContestModelMapper contestModelMapper, ContestEntryService contestEntryService, EntryModelMapper entryModelMapper, PhotoService photoService, PhotoModelMapper photoModelMapper, UserService userService, UserModelMapper userModelMapper) {
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
        this.contestModelMapper = contestModelMapper;
        this.contestEntryService = contestEntryService;
        this.entryModelMapper = entryModelMapper;
        this.photoService = photoService;
        this.photoModelMapper = photoModelMapper;
        this.userService = userService;
        this.userModelMapper = userModelMapper;
    }
    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isOrganizer")
    public boolean populateIsOrganizer(HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser1(session);
            return loggedUser.getUserRole().ordinal() == 1;
        } catch (AuthenticationException | EntityNotFoundException | NullPointerException e) {
            return false;
        }
    }

//    @GetMapping("/{contestId}")
//    public String showSingleContest(Model model, HttpSession session, @PathVariable int contestId) {
//
//        try {
//            var user = authenticationHelper.tryGetUser1(session);
//            var contest = contestService.getById(contestId);
//
//            model.addAttribute("form", new CreatePhotoDTO());
//            model.addAttribute("user", user);
//            model.addAttribute("contest", contest);
//
//
//        } catch (RuntimeException e) {
//            model.addAttribute("error", e.getMessage());
//            return "error";
//        }
//        return "single-contest";
//    }

    @GetMapping()
    public String showAllContests(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser1(session);
        } catch (AuthenticationException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("contests", contestService.getAll());
        return "all-contests";
    }

    @PostMapping()
    public String handleSearch(@ModelAttribute("search") Optional<String> search, HttpSession session, Model model) {
        List<Contest> contests = new ArrayList<>();

        try {
            authenticationHelper.tryGetUser1(session);
        } catch (AuthenticationException e) {
            return "redirect:/auth/login";
        }

        contests = contestService.searchByTitle(search);
        model.addAttribute("contests", contests);
        return "all-contests";
    }


    @GetMapping("/organiser/{id}/edit")
    public String getContestEditView(@PathVariable int id,
                                     Model model,
                                     HttpSession session) {

        Contest contest = contestService.getById(id);
        ContestOutputDTO contestOutputDto = contestModelMapper.contestToDto(contest);
        List<JuryDTO> possibleJury = userService.getAllJuries().stream()
                .map(userModelMapper::userToJuryDto)
                .collect(Collectors.toList());
        List<User> possibleParticipants = userService.getAllJunkies();
        boolean isInPhaseOne = contestOutputDto.getDaysPhase1().isAfter(LocalDate.now());

        model.addAttribute("contest", contestOutputDto);
        model.addAttribute("jury", possibleJury);
        model.addAttribute("participants", possibleParticipants);
        model.addAttribute("isInPhaseOne", isInPhaseOne);

        return "organiserEditContest";
    }

//    @PostMapping("/organiser/{id}/edit")
//    public String editContest(@PathVariable int id,
//                              @ModelAttribute("contest") ContestOutputDTO contestOutputDto,
//                              BindingResult errors, Model model, HttpSession session) {
//
//        Contest contest = contestService.getById(contestOutputDto.getId());
//        contestService.updateJuryAndParticipants(contest, contestOutputDto.getJuryList(), contestOutputDto.getParicipantList());
//
//        return "redirect:/contest/organiser/" + id + "/edit/";
//    }

    @GetMapping("/finished")
    public String showFinishedContests(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser1(session);
        } catch (AuthenticationException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("finishedContests", contestService.getFinishingContests());
        return "finishedContests";
    }

    @GetMapping("/open")
    public String showOpenContests(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser1(session);
        } catch (AuthenticationException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("openContests", contestService.getOpenContests());
        return "openContests";
    }

    @GetMapping("/enroll/{id}")
    public String viewEnrolPage(@PathVariable int id, Model model, HttpSession session) {
        ContestOutputDTO contest = contestModelMapper.contestToDto(contestService.getById(id));
        model.addAttribute("contest", contest);

        return "contestEnrolView";
    }

    @GetMapping("/enroll/{contestId}/{userId}")
    public String enrollContestant(@PathVariable int contestId, @PathVariable int userId) {
        contestService.addUserToContest(contestId, userId);
        return "redirect:/contests/" + contestId;
    }

    @GetMapping("/{id}")
    public String showSingleContest(@PathVariable int id, Model model, HttpSession session){
        model.addAttribute("participants",contestService.getContestParticipants(id));
        return "singleContest";
    }
}
