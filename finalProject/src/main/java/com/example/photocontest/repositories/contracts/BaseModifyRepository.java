package com.example.photocontest.repositories.contracts;

import com.example.photocontest.repositories.contracts.BaseGetRepository;

public interface BaseModifyRepository<T> extends BaseGetRepository<T> {
    void delete(int id);

    void update(T entity);

    void create(T entity);
}
