package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.ContestCategory;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface ContestCategoryRepository  {
    ContestCategory getById(int id);

    List<ContestCategory> getAllCategories();

    void addCategory(ContestCategory category);

    boolean categoryExist(ContestCategory category);
}
