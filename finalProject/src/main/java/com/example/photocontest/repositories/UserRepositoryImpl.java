package com.example.photocontest.repositories;

import com.example.photocontest.models.User;
import com.example.photocontest.models.enums.UserRole;
import com.example.photocontest.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.management.relation.Role;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl extends BaseModifyRepositoryImpl<User> implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory, SessionFactory sessionFactory1) {
        super(User.class, sessionFactory);
        this.sessionFactory = sessionFactory1;
    }

    @Override
    public User getByEmail(String email) {
        return getByField("email", email);
    }

    @Override
    public User getByUsername(String username) {
        return getByField("username", username);
    }


    @Override
    public List<User> searchByUserName(Optional<String> username) {
        if (username.isEmpty()) {
            throw new UnsupportedOperationException();
        }
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username.get());
            return query.list();
        }
    }

    @Override
    public List<User> getAllJuries() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User " +
                    "where userRole = :userRole and points > 150", User.class);
            query.setParameter("userRole", UserRole.valueOf("PHOTO_JUNKIE"));
            return query.getResultList();
        }
    }

    @Override
    public List<User> getAllOrganisers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User u where userRole = :userRole", User.class);
            query.setParameter("userRole", UserRole.valueOf("ORGANIZER"));
            return query.getResultList();
        }
    }

    @Override
    public List<User> getTopJunkies(int amount) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User ORDER BY score DESC", User.class);
            query.setMaxResults(amount);
            return query.getResultList();
        }
    }

    @Override
    public List<User> getAllJunkies() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User u where user.userRole = :userRole", User.class);
            query.setParameter("userRole", UserRole.PHOTO_JUNKIE);
            return query.getResultList();
        }
    }
}
