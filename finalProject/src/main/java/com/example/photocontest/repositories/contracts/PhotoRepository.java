package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.Photo;

import java.util.List;

public interface PhotoRepository extends BaseModifyRepository<Photo>{

    Photo getPhotoById(int id);

    Photo createPhoto(Photo photo);

    List<Photo> getAllPhotosByUserId(int userId);

    List<Photo> getAllPhotos();

}
