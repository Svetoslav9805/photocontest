package com.example.photocontest.repositories;

import com.example.photocontest.models.Review;
import com.example.photocontest.repositories.contracts.ReviewRepository;
import org.hibernate.Session;
import org.hibernate.SessionBuilder;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class ReviewRepositoryImpl extends BaseModifyRepositoryImpl<Review> implements ReviewRepository {

    @Autowired
    public ReviewRepositoryImpl(SessionFactory sessionFactory) {
        super(Review.class, sessionFactory);
    }



    @Override
    public Review getReviewByJuryIdAndPhotoId(int juryId, int photoId) {
        return null;
    }

    @Override
    public List<Review> getPhotoReviews(int id) {
        return null;
    }

    @Override
    public void deleteAllByPhotoId(int photoId) {

        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.createQuery("delete from Review where photo.id = :photoId")
                    .setParameter("photoId", photoId).executeUpdate();
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteAllByContestId(int contestId) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.createQuery("delete from Review where id = :contestId")
                    .setParameter("contestId", contestId).executeUpdate();
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Review> getAllReviewsByPhotoId(int photoId) {
        try (Session session = getSessionFactory().openSession()) {
            Query<Review> query =
                    session.createQuery("from Review where photo.id = :id", Review.class);
            query.setParameter("id", photoId);
            return query.getResultList();
        }
    }
}
