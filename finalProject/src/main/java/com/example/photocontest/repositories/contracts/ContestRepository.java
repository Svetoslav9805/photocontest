package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.Photo;
import com.example.photocontest.models.User;

import java.util.List;
import java.util.Optional;

public interface ContestRepository extends BaseModifyRepository<Contest> {
    List<Contest> getFinishedContests();

    Contest getById(int id);

    Contest getByTitle(String title);

    void updateContest(Contest contest);

    List<Contest> searchByTitle(Optional<String> contestTitle);

    List<Contest> getContestsInFirstPhase();

    List<Contest> getOpenContest();

    void updateContest1(Contest contest);


    List<User> getContestParticipants(int id);
}
