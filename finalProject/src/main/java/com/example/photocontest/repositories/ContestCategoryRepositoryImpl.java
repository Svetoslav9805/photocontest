package com.example.photocontest.repositories;

import com.example.photocontest.models.ContestCategory;
import com.example.photocontest.repositories.contracts.ContestCategoryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ContestCategoryRepositoryImpl extends BaseModifyRepositoryImpl<ContestCategory>
        implements ContestCategoryRepository {

    private final SessionFactory sessionFactory;

    public ContestCategoryRepositoryImpl(SessionFactory sessionFactory, SessionFactory sessionFactory1) {
        super(ContestCategory.class, sessionFactory);
        this.sessionFactory = sessionFactory1;
    }

    @Override
    public List<ContestCategory> getAllCategories() {
        return getAll();
    }

    @Override
    public void addCategory(ContestCategory category) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(category);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean categoryExist(ContestCategory category) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestCategory> query = session.createQuery("from ContestCategory " +
                    "where category = :categoryName", ContestCategory.class);
            query.setParameter("categoryName", category.getName());
            return !query.getResultList().isEmpty();
        }
    }
}
