package com.example.photocontest.repositories;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.repositories.contracts.BaseGetRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;


import java.util.List;

import static java.lang.String.format;

@Repository
public abstract class BaseGetRepositoryImpl<T> implements BaseGetRepository<T> {

    private final Class<T> clazz;
    private final SessionFactory sessionFactory;


    public BaseGetRepositoryImpl(Class<T> clazz, SessionFactory sessionFactory) {
        this.clazz = clazz;
        this.sessionFactory = sessionFactory;
    }

    public Class<T> getClazz() {
        return clazz;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            String query = String.format("from %s ", clazz.getSimpleName());
            return session.createQuery(query, clazz).getResultList();
        }
    }

    @Override
    public T getByField(String fieldName, String fieldValue) {
        try (Session session = sessionFactory.openSession()) {
            String query = format("from %s where %s = :%s", clazz.getSimpleName(), fieldName, fieldName);
            return session.createQuery(query, clazz)
                    .setParameter(fieldName, fieldValue)
                    .uniqueResultOptional()
                    .orElseThrow(() -> new EntityNotFoundException(clazz.getSimpleName(), fieldName, fieldValue));
        }
    }


    @Override
    public T getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            var query = format("from %s where id = :id", clazz.getSimpleName());
            return session.createQuery(query, clazz)
                    .setParameter("id", id)
                    .uniqueResultOptional()
                    .orElseThrow(() -> new EntityNotFoundException(clazz.getSimpleName(), id));
        }
    }




}
