package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.Review;

import java.util.List;

public interface ReviewRepository extends BaseModifyRepository<Review> {

    Review getReviewByJuryIdAndPhotoId(int juryId, int photoId);

    List<Review> getPhotoReviews(int id);

    void deleteAllByPhotoId(int photoId);

    void deleteAllByContestId(int contestId);

    List<Review> getAllReviewsByPhotoId(int photoId);
}
