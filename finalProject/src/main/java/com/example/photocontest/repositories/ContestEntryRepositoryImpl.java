package com.example.photocontest.repositories;

import com.example.photocontest.models.ContestEntry;
import com.example.photocontest.models.Photo;
import com.example.photocontest.repositories.contracts.ContestEntryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ContestEntryRepositoryImpl extends BaseModifyRepositoryImpl<ContestEntry> implements ContestEntryRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ContestEntryRepositoryImpl(SessionFactory sessionFactory) {
        super(ContestEntry.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<ContestEntry> getAllByContestId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestEntry> query = session.createQuery(
                    "FROM ContestEntry where contest.id = :id ", ContestEntry.class);
            query.setParameter("id", id);
            return query.list();
        }
    }

    @Override
    public void deleteAllByContestId(int contestId) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.createQuery("delete from ContestEntry where id = :contestId")
                    .setParameter("contestId", contestId).executeUpdate();
            session.getTransaction().commit();
        }
    }
}

