package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends BaseModifyRepository<User>{

    User getByEmail(String email);

    User getByUsername(String username);

    List<User> searchByUserName(Optional<String> username);

    List<User> getAllJuries();

    List<User> getAllOrganisers();

    List<User> getTopJunkies(int amount);

    List<User> getAllJunkies();
}
