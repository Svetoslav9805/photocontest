package com.example.photocontest.repositories;

import com.example.photocontest.models.Photo;
import com.example.photocontest.repositories.contracts.PhotoRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PhotoRepositoryImpl extends BaseModifyRepositoryImpl<Photo> implements PhotoRepository {


    public PhotoRepositoryImpl(SessionFactory sessionFactory) {
        super(PhotoRepository.class, sessionFactory);
    }

    @Override
    public Photo getPhotoById(int id) {
        return getById(id);
    }

    @Override
    public Photo createPhoto(Photo photo) {
        create(photo);
        return photo;
    }

    @Override
    public List<Photo> getAllPhotosByUserId(int userId) {
        try (Session session = getSessionFactory().openSession()) {
            Query<Photo> query = session.createQuery("FROM Photo p where author.id = :id ", Photo.class);
            query.setParameter("id", userId);
            return query.getResultList();
        }
    }

    @Override
    public List<Photo> getAllPhotos() {
        try (Session session = getSessionFactory().openSession()) {
            Query<Photo> query = session.createQuery("from Photo", Photo.class);
            return query.getResultList();
        }    }
}
