package com.example.photocontest.repositories;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.Photo;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.ContestRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public class ContestRepositoryImpl extends BaseModifyRepositoryImpl<Contest> implements ContestRepository {

    private final SessionFactory sessionFactory;

    public ContestRepositoryImpl(SessionFactory sessionFactory) {
        super(Contest.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Contest getByTitle(String title) {
        return getByField("title", title);
    }

    @Override
    public void updateContest(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(contest);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Contest> getFinishedContests() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("FROM Contest where finished = true", Contest.class);
            return query.getResultList();

        }
    }

    @Override
    public List<Contest> searchByTitle(Optional<String> title) {
        if (title.isEmpty()) {
            throw new UnsupportedOperationException();
        }
        try (Session session = sessionFactory.openSession()) {
//            Query<Post> query = session.createQuery("from Post where postTitle = :postTitle", Post.class);
            Query<Contest> query = session.createQuery("from Contest where title like :title", Contest.class);
            query.setParameter("title", "%" +  title.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<Contest> getContestsInFirstPhase() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query =
                    session.createQuery("from Contest where phase_1 > :date", Contest.class);
            query.setParameter("date", LocalDateTime.now());
            return query.getResultList();

        }
    }

    @Override
    public List<Contest> getOpenContest() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("FROM Contest where isOpen = true", Contest.class);
            return query.getResultList();
        }
    }

    @Override
    public void updateContest1(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

//            for (Photo photo : contest.getPhotoSet()) {
//                session.update(photo);
//            }

            session.update(contest);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<User> getContestParticipants(int id) {
        try (Session session = sessionFactory.openSession()) {

//
//                Query<User> query = session.createQuery("FROM User u join ContestParticipant cp where  ", User.class);
//                return query.getResultList();

            NativeQuery query = session.createNativeQuery(
                    "select distinct u.id, first_name, last_name, username, password, email, role, score from photo_contest.users u" +
                    "join photo_contest.contests_participants cp on u.id = cp.user_id" +
                  "where cp.contest_id = :id");
            query.setParameter("id", id);
            query.addEntity(User.class);
            return query.list();
        }
    }

}
