package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.ContestEntry;
import com.example.photocontest.models.Photo;

import java.util.List;

public interface ContestEntryRepository extends BaseModifyRepository<ContestEntry> {


    List <ContestEntry> getAllByContestId(int id);

    void deleteAllByContestId(int contestId);
}
