package com.example.photocontest.repositories;

import com.example.photocontest.repositories.contracts.BaseModifyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.parser.Entity;

@Repository
public abstract class BaseModifyRepositoryImpl<T> extends BaseGetRepositoryImpl<T> implements BaseModifyRepository<T> {

    public BaseModifyRepositoryImpl(Class clazz, SessionFactory sessionFactory) {
        super(clazz, sessionFactory);
    }

    @Override
    public void create(Object entity) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Object entity) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        T toDelete = getById(id);
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.delete(toDelete);
            session.getTransaction().commit();
        }
    }
}
