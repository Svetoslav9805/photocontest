package com.example.photocontest.exceptions;

public class DateTimeException extends RuntimeException{

    public DateTimeException() {
        super("You can only give reviews during phase two.");
    }
}
