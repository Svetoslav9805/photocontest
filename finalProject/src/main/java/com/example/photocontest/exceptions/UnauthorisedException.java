package com.example.photocontest.exceptions;

public class UnauthorisedException extends RuntimeException{
    public UnauthorisedException(String message) { super(message); }
}
