package com.example.photocontest.utils;

import com.example.photocontest.exceptions.UnauthorisedException;
import com.example.photocontest.models.User;
import com.example.photocontest.models.enums.UserRole;
import org.springframework.stereotype.Component;

@Component
public class AuthorizationChecker {

    private static final String NOT_OWNER_OR_ADMIN = "Only organizers or the creator can change this content.";
    private static final String UNAUTHORISED_USER = "Only organizers can preform this operation.";


    public void authorizedOrOwner(int id, User user) {
        boolean userIsOwner = user.getUserId() == id;
        boolean userIsNotAdmin = user.getUserRole() != UserRole.ORGANIZER;
        if (!userIsOwner && userIsNotAdmin) {
            throw new UnauthorisedException(NOT_OWNER_OR_ADMIN);
        }
    }
    //TODO: Има възможност - проверката да бъде "Или". Ако е owner е админ хвърля грешка. Гърми когато двете са истина.

    public  void authorize(User user) {
        boolean userIsNotAdmin = user.getUserRole() != UserRole.ORGANIZER;
        if (userIsNotAdmin) {
            throw new UnauthorisedException(UNAUTHORISED_USER);
        }
    }


}
