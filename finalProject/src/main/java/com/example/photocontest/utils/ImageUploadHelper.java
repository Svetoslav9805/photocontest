package com.example.photocontest.utils;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Component
public class ImageUploadHelper {

    private final Cloudinary cloudinary;

    public ImageUploadHelper() {
        this.cloudinary = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", "telerikphotoprojectfour",
                "api_key", "568812257213527",
                "api_secret", "436Pj9jd5o9w4od9cbDpshl_-9I",
                "secure", true));;
    }

    public String upload(MultipartFile multipartFile) throws IOException {
        var upload_result = cloudinary.uploader()
                .cloudinary()
                .uploader()
                .upload(multipartFile.getBytes(),ObjectUtils.emptyMap());
        return upload_result.get("url").toString();
    }

//    public void delete(String publicID) throws IOException {
//        cloudinary.uploader().destroy(publicID, Map.of());
//    }
}
