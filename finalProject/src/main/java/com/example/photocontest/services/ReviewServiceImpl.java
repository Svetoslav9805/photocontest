package com.example.photocontest.services;

import com.example.photocontest.exceptions.UnauthorisedException;
import com.example.photocontest.models.Review;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.ReviewRepository;
import com.example.photocontest.services.contracts.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReviewServiceImpl implements ReviewService {
    private final ReviewRepository reviewRepository;

    @Autowired
    public ReviewServiceImpl(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    @Override
    public List<Review> getAllReviews() {
        return reviewRepository.getAll();
    }

    @Override
    public Review getReviewById(int id) {
        return reviewRepository.getById(id);
    }

    @Override
    public Review updateReview(Review review, User user) {
        if(!review.getUser().equals(user)){
            throw new UnauthorisedException("You cannot give reviews.");
        }
        return review;
    }

    @Override
    public List<Review> getPhotoReviews(int photoId) {
        return reviewRepository.getPhotoReviews(photoId);
    }

    @Override
    public Review getReviewByJuryIdAndPhotoId(int juryId, int photoId) {
        return reviewRepository.getReviewByJuryIdAndPhotoId(juryId, photoId);
    }

    @Override
    public void create(Review review) {
        reviewRepository.create(review);
    }

    @Override
    public void deleteAllByContestId(int contestId) {
        reviewRepository.deleteAllByContestId(contestId);
    }

    @Override
    public List<Review> getAllReviewsByPhotoId(int entryId) {
        return reviewRepository.getAllReviewsByPhotoId(entryId);
    }


}
