package com.example.photocontest.services;

import com.example.photocontest.exceptions.DuplicateEntityException;
import com.example.photocontest.models.ContestCategory;
import com.example.photocontest.repositories.contracts.ContestCategoryRepository;
import com.example.photocontest.services.contracts.ContestCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContestCategoryServiceImpl implements ContestCategoryService {

    private final ContestCategoryRepository contestCategoryRepository;

    @Autowired
    public ContestCategoryServiceImpl(ContestCategoryRepository contestCategoryRepository) {
        this.contestCategoryRepository = contestCategoryRepository;
    }

    @Override
    public ContestCategory getById(int id) {
        return contestCategoryRepository.getById(id);
    }

    @Override
    public List<ContestCategory> getAllCategories() {
        return contestCategoryRepository.getAllCategories();
    }

    @Override
    public void addCategory(ContestCategory category) {
        if (exist(category)) {
            throw new DuplicateEntityException("Category exist");
        }
        contestCategoryRepository.addCategory(category);
    }

    @Override
    public boolean exist(ContestCategory category) {
        return contestCategoryRepository.categoryExist(category);
    }
}
