package com.example.photocontest.services;

import com.example.photocontest.exceptions.DuplicateEntityException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorisedException;
import com.example.photocontest.models.User;
import com.example.photocontest.models.enums.UserRole;
import com.example.photocontest.repositories.contracts.UserRepository;
import com.example.photocontest.services.contracts.UserService;
import com.example.photocontest.utils.AuthorizationChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.example.photocontest.utils.Constants.ORGANIZER_PROMOTE_ERROR_MESSAGE;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final AuthorizationChecker authorizationChecker;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, AuthorizationChecker authorizationChecker) {
        this.userRepository = userRepository;
        this.authorizationChecker = authorizationChecker;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByField("username", username);
    }

    @Override
    public User create(User user) {
        boolean duplicateExists = true;
        try {
            userRepository.getByField("email", user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }
        userRepository.create(user);
        return user;
    }

    @Override
    public void update(User userToUpdate, User updatingUser) {
        authorizationChecker.authorizedOrOwner(userToUpdate.getUserId(), updatingUser);
        userRepository.update(userToUpdate);
    }

    @Override
    public void update(User user) {
        userRepository.update(user);
    }

    @Override
    public List<User> searchByUsername(Optional<String> username) {
        return userRepository.searchByUserName(username);
    }

    @Override
    public List<User> getAllJuries() {
        return userRepository.getAllJuries();
    }

    @Override
    public List<User> getAllOrganisers() {
        return userRepository.getAllOrganisers();
    }

    @Override
    public User promote(User loggedUser, User userToPromote) {
        checkAuthorization(loggedUser, ORGANIZER_PROMOTE_ERROR_MESSAGE);
        userToPromote.setUserRole(UserRole.ORGANIZER);
        userRepository.update(userToPromote);
        return userToPromote;
    }

    @Override
    public List<User> getTopJunkies(int amountJunkies) {
        return userRepository.getTopJunkies(amountJunkies);
    }

    private void checkAuthorization(User loggedUser, String message) {
        if (loggedUser.getUserRole().ordinal() != 1) {
            throw new UnauthorisedException(message);
        }
    }

    @Override
    public List<User> getAllJunkies() {
        return userRepository.getAllJunkies();
    }
}
