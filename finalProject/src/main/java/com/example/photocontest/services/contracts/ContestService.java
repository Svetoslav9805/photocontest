package com.example.photocontest.services.contracts;

import com.example.photocontest.models.Photo;
import com.example.photocontest.models.User;
import com.example.photocontest.models.Contest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface ContestService {
    Contest getById(int id);

    void createContest(Contest contest, MultipartFile multipartFile, User user);

    List<Contest> getFinishingContests();

    void updateContest(Contest contest);

    void rankPhoto(List<Photo> firstPlacePhotos,
                   List<Photo> secondPlacePhotos,
                   List<Photo> thirdPlacePhotos,
                   Photo photo);

    List<Contest> getAll();

    List<Contest> searchByTitle(Optional<String> title);

    void create(Contest contest);

    public List<Contest> getContestsInFirstPhase();

    List<Contest> getOpenContests();

    void deleteContest(int contestId);
    void addUserToContest(int id, int userId);

    List<User> getContestParticipants(int id);

}
