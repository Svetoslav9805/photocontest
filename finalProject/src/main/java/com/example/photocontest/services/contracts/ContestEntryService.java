package com.example.photocontest.services.contracts;

import com.example.photocontest.models.ContestEntry;
import com.example.photocontest.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ContestEntryService {

    void createEntry(ContestEntry entry, MultipartFile image, User user);

    ContestEntry getById (int entryId);

    void updateEntry(ContestEntry entry, User user);

    void deleteEntry(int entryId, User user);

    List<ContestEntry> getAllByContestId(int id);

    void deleteAllByContestId(int contestId);
}
