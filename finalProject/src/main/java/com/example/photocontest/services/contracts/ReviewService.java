package com.example.photocontest.services.contracts;

import com.example.photocontest.models.Review;
import com.example.photocontest.models.User;

import java.util.List;

public interface ReviewService {

    List<Review> getAllReviews();

    Review getReviewById(int id);

    Review updateReview(Review review, User user);

    List<Review> getPhotoReviews(int photoId);

    Review getReviewByJuryIdAndPhotoId(int juryId, int photoId);

    void create(Review review);

    void deleteAllByContestId(int contestId);

    List<Review> getAllReviewsByPhotoId(int entryId);
}
