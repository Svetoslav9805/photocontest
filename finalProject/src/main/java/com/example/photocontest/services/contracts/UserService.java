package com.example.photocontest.services.contracts;

import com.example.photocontest.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAll();

    User getById(int id);

    User getByEmail(String email);

    User getByUsername(String username);

    User create(User user);

    void update (User updatingUser, User userToUpdate);

    void update(User user);

    List<User> searchByUsername(Optional<String> username);

    List<User> getAllJuries();

    List<User> getAllOrganisers();

    User promote(User loggedUser, User userToPromote);

    List<User> getTopJunkies(int amountJunkies);

    List<User> getAllJunkies();
}
