package com.example.photocontest.services;

import com.example.photocontest.models.Photo;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.PhotoRepository;
import com.example.photocontest.services.contracts.PhotoService;
import com.example.photocontest.services.contracts.UserService;
import com.example.photocontest.utils.ImageUploadHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class PhotoServiceImpl implements PhotoService {

    private final PhotoRepository photoRepository;
    private final ImageUploadHelper imageUploadHelper;
    private final UserService userService;

    @Autowired
    public PhotoServiceImpl(PhotoRepository photoRepository,
                            ImageUploadHelper imageUploadHelper,
                            UserService userService) {
        this.photoRepository = photoRepository;
        this.imageUploadHelper = imageUploadHelper;
        this.userService = userService;
    }

    @Override
    public Photo createPhoto(MultipartFile multipartFile, User user) throws IOException {
        String url = imageUploadHelper.upload(multipartFile);
        Photo photo = new Photo();
        photo.setFilePath(url);
        photo.setAuthor(userService.getById(user.getUserId()));
        return photoRepository.createPhoto(photo);
        //вариант 2
        //get by id
        //setAuthor
        //update
        // if null --> exception // if null --> default photo
    }

    @Override
    public List<Photo> getAll() {
        return photoRepository.getAllPhotos();
    }

    @Override
    public Photo getById(int id) {
        return photoRepository.getById(id);
    }

}
