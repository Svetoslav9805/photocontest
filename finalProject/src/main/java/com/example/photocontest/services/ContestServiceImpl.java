package com.example.photocontest.services;

import com.example.photocontest.exceptions.DuplicateEntityException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorisedException;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.Photo;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.ContestRepository;
import com.example.photocontest.services.contracts.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContestServiceImpl implements ContestService {

    private final UserService userService;
    private final PhotoService photoService;
    private final ReviewService reviewService;
    private final ContestRepository contestRepository;
    private final ContestEntryService contestEntryService;

    public ContestServiceImpl(UserService userService,
                              PhotoService photoService,
                              ReviewService reviewService,
                              ContestRepository contestRepository,
                              ContestEntryService contestEntryService) {
        this.userService = userService;
        this.photoService = photoService;
        this.reviewService = reviewService;
        this.contestRepository = contestRepository;
        this.contestEntryService = contestEntryService;
    }


    @Override
    public Contest getById(int id) {
        return contestRepository.getById(id);
    }

    @Override
    public void createContest(Contest contest, MultipartFile multipartFile, User user) {
        try {
            Photo contestCoverPhoto = photoService.createPhoto(multipartFile, user);
            contest.setCoverPhoto(contestCoverPhoto.getFilePath());
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
        contestRepository.create(contest);
    }

    @Override
    public void deleteContest(int contestId) {
        Contest contest = contestRepository.getById(contestId);
        contestEntryService.deleteAllByContestId(contestId);
        reviewService.deleteAllByContestId(contestId);
        contestRepository.delete(contestId);
    }

    @Override
    public void updateContest(Contest contest) {
        contestRepository.updateContest(contest);
    }

    @Override
    public void rankPhoto(List<Photo> firstPlacePhotos,
                          List<Photo> secondPlacePhotos,
                          List<Photo> thirdPlacePhotos,
                          Photo photo) {
        if (firstPlacePhotos.isEmpty()) {
            firstPlacePhotos.add(photo);
            return;
        }
        if (photo.getPoints() == firstPlacePhotos.get(0).getPoints()) {
            firstPlacePhotos.add(photo);
            return;
        }
        if (secondPlacePhotos.isEmpty()) {
            secondPlacePhotos.add(photo);
            return;
        }
        if (photo.getPoints() == secondPlacePhotos.get(0).getPoints()) {
            secondPlacePhotos.add(photo);
            return;
        }
        if (thirdPlacePhotos.isEmpty()) {
            thirdPlacePhotos.add(photo);
            return;
        }
        if (photo.getPoints() == thirdPlacePhotos.get(0).getPoints()) {
            thirdPlacePhotos.add(photo);
        }

    }

    @Override
    public List<Contest> getAll() {
        return contestRepository.getAll();
    }

    @Override
    public List<Contest> searchByTitle(Optional<String> title) {
        return contestRepository.searchByTitle(title);
    }

    @Override
    public void create(Contest contest) {
        List<User> organiserJury = userService.getAllOrganisers();
        contest.getJurySet().addAll(organiserJury);
        if (contest.getPhase_1().isBefore(LocalDateTime.now())) {
            throw new DateTimeException("Contest cannot be set in the past");
        }
        if (contest.getPhase_1().isAfter(LocalDateTime.now().plusMonths(1))) {
            throw new DateTimeException("Length of contest cannot be more than one month");
        }
        contestRepository.create(contest);
    }

    @Override
    public List<Contest> getContestsInFirstPhase() {
        return contestRepository.getContestsInFirstPhase();
    }

    @Override
    public List<Contest> getFinishingContests() {
        return contestRepository.getFinishedContests();
    }

    @Override
    public List<Contest> getOpenContests() {
        return contestRepository.getOpenContest();
    }


    @Override
    public void addUserToContest(int id, int userId) {
        User user = userService.getById(userId);
        Contest contest = getById(id);
        if (!contest.getOpen()) {
            throw new UnauthorisedException("You cannot add new users to a closed contest");
        }
        if (contest.getJurySet().contains(user)) {
            throw new UnauthorisedException("You cannot add a jury to the contest");
        }
        if (contest.getParticipantsSet().contains(user)) {
            throw new DuplicateEntityException("User is already in the competition");
        }
        contest.getParticipantsSet().add(user);

        contestRepository.updateContest1(contest);
    }

    @Override
    public List<User> getContestParticipants(int id) {
        try {
            return contestRepository.getContestParticipants(id);
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }
    }
