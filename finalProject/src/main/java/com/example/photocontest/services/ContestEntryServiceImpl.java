package com.example.photocontest.services;

import com.example.photocontest.models.ContestEntry;
import com.example.photocontest.models.Photo;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.ContestEntryRepository;
import com.example.photocontest.repositories.contracts.ReviewRepository;
import com.example.photocontest.services.contracts.ContestEntryService;
import com.example.photocontest.services.contracts.PhotoService;
import com.example.photocontest.utils.AuthorizationChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class ContestEntryServiceImpl implements ContestEntryService {

    private final PhotoService photoService;
    private final ReviewRepository reviewRepository;
    private final AuthorizationChecker authorizationChecker;
    private final ContestEntryRepository contestEntryRepository;

    @Autowired
    public ContestEntryServiceImpl(PhotoService photoService,
                                   ReviewRepository reviewRepository,
                                   AuthorizationChecker authorizationChecker,
                                   ContestEntryRepository contestEntryRepository) {
        this.photoService = photoService;
        this.reviewRepository = reviewRepository;
        this.authorizationChecker = authorizationChecker;
        this.contestEntryRepository = contestEntryRepository;
    }

    @Override
    public void createEntry(ContestEntry entry, MultipartFile image, User user) {
        try {
            Photo entryPhoto = photoService.createPhoto(image, user);
            entry.setPhoto(entryPhoto);
            contestEntryRepository.create(entry);
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public ContestEntry getById(int entryId) {
        return contestEntryRepository.getById(entryId);
    }

    @Override
    public void updateEntry(ContestEntry entry, User user) {
        authorizationChecker.authorizedOrOwner(entry.getUser().getUserId(), user);
        ContestEntry entryToUpdate = contestEntryRepository.getById(entry.getEntry_id());
        contestEntryRepository.update(entryToUpdate);
    }

    @Override
    public void deleteEntry(int entryId, User user) {
        ContestEntry entry = contestEntryRepository.getById(entryId);
        authorizationChecker.authorizedOrOwner(entry.getUser().getUserId(), user);
        reviewRepository.deleteAllByPhotoId(entry.getPhoto().getPhotoId());
        contestEntryRepository.delete(entryId);
    }

    @Override
    public List<ContestEntry> getAllByContestId(int id) {
        return contestEntryRepository.getAllByContestId(id);
    }

    @Override
    public void deleteAllByContestId(int contestId) {
        contestEntryRepository.deleteAllByContestId(contestId);
    }

}


