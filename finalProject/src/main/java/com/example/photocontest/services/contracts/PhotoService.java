package com.example.photocontest.services.contracts;

import com.example.photocontest.models.Photo;
import com.example.photocontest.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface PhotoService {

//    void createPhoto(Photo photo);
    Photo createPhoto(MultipartFile multipartFile, User user) throws IOException;

    List<Photo> getAll();

    Photo getById(int id);
}
