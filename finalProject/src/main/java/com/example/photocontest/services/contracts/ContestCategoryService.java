package com.example.photocontest.services.contracts;

import com.example.photocontest.models.ContestCategory;

import java.util.List;

public interface ContestCategoryService {

    ContestCategory getById(int id);

    List<ContestCategory> getAllCategories();

   void addCategory(ContestCategory category);

    boolean exist(ContestCategory category);

}
