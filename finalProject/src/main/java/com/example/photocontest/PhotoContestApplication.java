package com.example.photocontest;

import com.cloudinary.utils.ObjectUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.cloudinary.*;


@SpringBootApplication
public class PhotoContestApplication {

    public static void main(String[] args) {
        SpringApplication.run(PhotoContestApplication.class, args);
    }
}
