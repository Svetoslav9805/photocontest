package com.example.photocontest.services;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestCategory;
import com.example.photocontest.models.Photo;
import com.example.photocontest.models.User;

import java.util.ArrayList;
import java.util.HashSet;

public class Helper {
    public static ContestCategory createCategory() {
        ContestCategory category = new ContestCategory();
        category.setId(1);
        category.setName("testCategory");
        return category;
    }

    public static Contest createContest() {
        Contest contest = new Contest();
        contest.setId(1);
        contest.setTitle("Title");
       // contest.setCategory(createCategory());
        contest.setJurySet(new HashSet<>());
        contest.setParticipantsSet(new HashSet<>());
        contest.setContestPhotos(new HashSet<>());
        contest.setFinished(false);
        contest.setOpen(true);
       // contest.setCoverPhotoPath("testPath");
       // contest.setTimeLimitPhase1(LocalDateTime.now());
       // contest.setTimeLimitPhase2(LocalDateTime.now());
        return contest;
    }

    public static User createUser() {
        User user = new User();
        user.setUserId(1);
        user.setFirstName("Firstname");
        user.setLastName("LastName");
        user.setPoints(1);
        return user;
    }

    public static User createUser1(int id) {
        User user = new User();
        user.setUserId(id);
        user.setFirstName("Firstname");
        user.setLastName("LastName");
        user.setPoints(0);
        return user;
    }



    public static Photo createPhoto(){
        Photo photo = new Photo();
        photo.setPhotoId(1);
        photo.setReviewSet(new ArrayList<>());
        photo.setAuthor(createUser());
        photo.setFilePath("filePath");
        return photo;
    }

    public static Photo createPhoto(int id){
        Photo photo = new Photo();
        photo.setPhotoId(id);
        photo.setReviewSet(new ArrayList<>());
        photo.setAuthor(createUser());
        photo.setFilePath("filePath");
        return photo;
    }

//    public static Review createReview(){
//        Review review = new Review();
//        review.setId(1);
//        review.setUser(createUser());
//        review.setScore(3);
//        review.setComment("NoComment");
//        review.setEdited(false);
//        review.setPhoto(createPhoto());
//        return review;
//    }
//
//    public static Review createReview(int points){
//        Review review = new Review();
//        review.setId(1);
//        review.setUser(createUser());
//        review.setScore(points);
//        review.setComment("NoComment");
//        review.setEdited(false);
//        review.setPhoto(createPhoto());
//        return review;
//    }
}
