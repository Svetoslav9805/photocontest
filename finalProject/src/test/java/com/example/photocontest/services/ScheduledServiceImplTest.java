package com.example.photocontest.services;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.Photo;
import com.example.photocontest.models.Review;
import com.example.photocontest.models.User;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.services.contracts.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.example.photocontest.services.Helper.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.ExpectedCount.times;

@ExtendWith(MockitoExtension.class)
public class ScheduledServiceImplTest {
    private Contest contest;

    ContestService contestService;
    UserService userService;

    @InjectMocks
    ScheduledService scheduledService;

    @BeforeEach
    private void makeContest() {
        contest = createContest();
        User user = createUser();
        Photo photo = createPhoto(1);
//        Review review = createReview();
//        photo.getReviewSet().add(review);
        photo.setAuthor(user);
        contest.getContestPhotos().add(photo);
    }


    @Test
    public void updateRankingPoints_Should_Call_Repository_When_Valid() {
        List<Contest> contestList = new ArrayList<>(Arrays.asList(createContest(), createContest()));
        when(contestService.getFinishingContests()).thenReturn(contestList);

        scheduledService.updateRankingPoints();
//        verify(contestService, times(2)).updateContest(createContest());

    }

    @Test
    public void updateRankingPoints_Should_Change_Finished_Field_ToTrue() {
        Contest contest = createContest();
        contest.setFinished(false);
        List<Contest> contestList = new ArrayList<>(Arrays.asList(createContest(), contest));
        when(contestService.getFinishingContests()).thenReturn(contestList);

        scheduledService.updateRankingPoints();

        assertTrue(contest.isFinished());
    }
}
