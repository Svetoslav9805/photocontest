create or replace table contests_categories
(
    id       int auto_increment
        primary key,
    category varchar(50) not null,
    constraint categories_category_name_uindex
        unique (category)
);

create or replace table contests
(
    id                 int auto_increment
        primary key,
    title              varchar(50) not null,
    category_id        int         not null,
    time_limit_phase_1 date        not null,
    time_limit_phase_2 datetime    not null,
    is_open            tinyint(1)  not null,
    is_finished        tinyint(1)  not null,
    cover_photo        text        not null,
    constraint contests_title_uindex
        unique (title),
    constraint contests_categories_id_fk
        foreign key (category_id) references contests_categories (id)
);

create or replace table users
(
    id         int auto_increment
        primary key,
    first_name varchar(20) not null,
    last_name  varchar(20) not null,
    username   varchar(40) not null,
    password   varchar(40) not null,
    email      varchar(40) not null,
    role       int         not null,
    score      int         not null
);

create or replace table contests_juries
(
    id         int auto_increment
        primary key,
    contest_id int not null,
    user_id    int not null,
    constraint contests_jury_contests_id_fk
        foreign key (contest_id) references contests (id),
    constraint contests_jury_users_id_fk
        foreign key (user_id) references users (id)
);

create or replace table contests_participants
(
    id         int auto_increment
        primary key,
    contest_id int not null,
    user_id    int not null,
    constraint contest_perticipants_contests_id_fk
        foreign key (contest_id) references contests (id),
    constraint contest_perticipants_users_id_fk
        foreign key (user_id) references users (id)
);

create or replace table photo
(
    id        int auto_increment
        primary key,
    file_path text not null,
    user_id   int  null,
    constraint photos_file_path_uindex
        unique (file_path) using hash,
    constraint photos_users_id_fk
        foreign key (user_id) references users (id)
);

create or replace table contest_entries
(
    entry_id   int auto_increment
        primary key,
    title      varchar(64)  not null,
    story      varchar(600) null,
    contest_id int          not null,
    user_id    int          not null,
    photo_id   int          not null,
    constraint contest_entries_entry_id_uindex
        unique (entry_id),
    constraint contest_entries_photo_link_uindex
        unique (photo_id),
    constraint contest_entries_contests_id_fk_2
        foreign key (contest_id) references contests (id),
    constraint contest_entries_photo_id_fk
        foreign key (photo_id) references photo (id)
);

create or replace index contest_entries_users_id_fk
    on contest_entries (user_id);

create or replace table reviews
(
    id         int auto_increment
        primary key,
    score      int  not null,
    comment    text null,
    photo_id   int  not null,
    user_id    int  not null,
    contest_id int  not null,
    constraint reviews_contest_id_uindex
        unique (contest_id),
    constraint reviews_user_id_uindex
        unique (user_id),
    constraint reviews_contests_id_fk
        foreign key (contest_id) references contests (id),
    constraint reviews_photos_id_fk
        foreign key (photo_id) references photo (id),
    constraint reviews_users_id_fk
        foreign key (user_id) references users (id)
);

